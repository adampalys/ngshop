(function(){
	'use strict'

	angular
		.module('app')
		.controller('NavigationController', NavigationController);

	NavigationController.$inject = ['$scope','$location', 'cartFactory'] ;

	function NavigationController($scope, $location, cartFactory){
		
		var vm = this;
		vm.isActive = isActive;
		vm.navigation = navigation;

		$scope.$watch(function(){
			vm.cart = cartFactory.getCart().length;
		});

		function isActive(path){
			return $location.path() === path;
		}

		function navigation(){
			if ( /^\/admin/.test( $location.path() ) )
				return 'app/layout/admin-navigation.html';
			else
				return 'app/layout/navigation.html';
		}

	}

})();