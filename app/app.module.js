'use strict';

angular
	.module( 'app' , [
		'ngRoute',
		'angular-storage',
		'app.admin',
		'app.site'
	]);