(function(){

	'use strict'

	angular
		.module('admin.orders')
		.controller('OrdersAdminController', OrdersAdminController);

	OrdersAdminController.$inject = ['orders'];

	function OrdersAdminController(orders){
		var vm = this;
		
		vm.orders = orders;
		vm.changeStatus = changeStatus;
		vm.removeOrder = removeOrder;

		function changeStatus( order ) {
			order.status = !order.status;
			// TODO: API
		};

		function removeOrder( user , $index ) {
			if(!confirm('Czy na pewno chcesz usunąć to zamówienie?'))
				return false;
			vm.orders.splice( $index , 1 );
			// TODO: API
		};
	}

})();