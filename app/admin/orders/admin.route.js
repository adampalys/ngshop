(function(){
 
    'use strict';

    angular
        .module('app')
        .config(config);

    function config( $routeProvider , $httpProvider ){

        $routeProvider
            .when('/admin/orders', {
                controller: 'OrdersAdminController',
                controllerAs: 'vm',
                templateUrl: 'app/admin/orders/orders.html',
                resolve: {
                    orders: ['ordersService', function (ordersService){
                        return ordersService.getOrders();
                    }]
                }
        });
    }
    
})();