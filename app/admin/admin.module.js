'use strict'

angular
	.module('app.admin', [
		'ngRoute',
		'admin.orders',
		'admin.products',
		'admin.users'
	]);