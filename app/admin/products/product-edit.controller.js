(function(){
 
	'use strict';

	angular
        .module('admin.products')
        .controller('ProductEditAdminController', ProductEditAdminController);

	ProductEditAdminController.$inject = ['product'];

	function ProductEditAdminController(product){

	    var vm = this;
	    vm.product = product;
	    vm.saveChanges = saveChanges;

	    function saveChanges( product ) {
	        // TODO: API
	        // console.log( product );
	        // console.log( $routeParams.id ); // inject route
	    }
	}
 
})();