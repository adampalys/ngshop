(function(){

	'use strict'

	angular
		.module('admin.products')
		.controller('ProductsAdminController', ProductsAdminController);

	ProductsAdminController.$inject = ['products'];

	function ProductsAdminController(products){

		var vm = this;
		vm.products = products;
		vm.removeProduct = removeProduct;

		function removeProduct(product, $index){
			if(!confirm('Czy na pewno chcesz usunąć ten produkt?'))
				return false;
			vm.products.splice( $index , 1 );
			//TODO: API
		}
	}

})();
