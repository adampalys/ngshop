(function(){
 
    'use strict';

    angular
        .module('app')
        .config(config);

    function config( $routeProvider , $httpProvider ){

    	$routeProvider
	        .when( '/admin/products' , {
                controller : 'ProductsAdminController',
                controllerAs: 'vm',
                templateUrl : 'app/admin/products/products.html',
                resolve:{
                    products: ['productsService', function (productsService){
                        return productsService.getProducts();
                    }]
                }
        });

        $routeProvider
            .when( '/admin/product/edit/:id' , {
                controller: 'ProductEditAdminController',
                controllerAs: 'vm',
                templateUrl : 'app/admin/products/product-edit.html',
                resolve:{
                    product: ['productsService', '$route', function (productsService, $route){
                        return productsService.getProduct($route.current.params.id);
                    }]
                }
        });

        $routeProvider
            .when( '/admin/product/create' , {
                controller: 'ProductCreateAdminController',
                controllerAs: 'vm',
                templateUrl : 'app/admin/products/product-create.html'
        });

	}

})();