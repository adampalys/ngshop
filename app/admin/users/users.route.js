(function(){
 
    'use strict';

    angular
        .module('app')
        .config(config);

    function config( $routeProvider , $httpProvider ){

    	$routeProvider
            .when('/admin/users',{
                controller: 'UsersAdminController',
                controllerAs: 'vm',
                templateUrl: 'app/admin/users/users.html',
                resolve:{
                    users: ['usersService', function (usersService){
                        return usersService.getUsers();
                    }]
                }
        });

        $routeProvider
            .when('/admin/user/edit/:id' , {
                controller: 'UserEditAdminController',
                controllerAs: 'vm',
                templateUrl: 'app/admin/users/user-edit.html',
                resolve:{
                    user: ['usersService', '$route', function (usersService, $route){
                        return usersService.getUser($route.current.params.id);
                    }]
                }
        });

        $routeProvider
            .when('/admin/user/create', {
                controller: 'UserAdminCreateController',
                controllerAs: 'vm',
                templateUrl: 'app/admin/users/user-create.html'
        });
    	
    }

})();