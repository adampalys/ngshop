(function(){
	
	'use strict'

	angular
		.module('admin.users')
		.controller('UsersAdminController', UsersAdminController);

	UsersAdminController.$inject = ['users'];

	function UsersAdminController(users){
		
		var vm = this;
		vm.users = users;
		vm.removeUser = removeUser;

		function removeUser(user, $index){
			if(!confirm('Czy na pewno chcesz usunąć użytkownika?'))
				return false;
			vm.users.splice($index,1);
		}
	}

})();