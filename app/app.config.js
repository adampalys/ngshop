(function(){
 
    'use strict';

    angular
        .module('app')
        .config(config);

    function config( $routeProvider , $httpProvider ){

        $routeProvider
	        .otherwise({
                redirectTo: '/products'
        });
    }
    
})();