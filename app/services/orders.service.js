(function(){

	'use strict'

	angular
		.module('app')
		.service('ordersService', ordersService);

	ordersService.$inject = ['$http'];

	function ordersService($http){

		var services = {
			getOrders: getOrders
		};

		return services;

		function getOrders(){
			return $http.get('model/orders.json').then(success, error);

			function success(orders){
				return orders.data;
			}

			function error(){
				console.log('Błąd pobrania pliku json');
			}
		}

	}

})();