(function(){

    'use strict'

    angular
	    .module('app')
	    .service('productsService', productsService);

    productsService.$inject = ['$http'];

    function productsService($http){

        var services = {
                getProduct: getProduct,
                getProducts: getProducts
        }

        return services;

        function getProduct(index){
            return $http.get( 'model/products.json' ).then(success,error);

            function success(items){
                return items.data[index];
            }

            function error(){
                console.log( 'Błąd pobrania pliku json' );
            }
        }

	    function getProducts(){
            return $http.get( 'model/products.json' ).then(success,error);

            function success(items){
                return items.data;
            }

            function error(){
                console.log( 'Błąd pobrania pliku json' );
            }
        }
        
    }

})();