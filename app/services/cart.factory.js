(function(){

	'use strict'

	angular
		.module('app')
		.factory('cartFactory', cartFactory);

	cartFactory.$inject = ['store'];

	function cartFactory(store){

		if ( store.get( 'cart' ) ){
			var cart = store.get( 'cart' );
		}
		else{
			var cart = [];
		}

		var services = {
			getCart: getCart,
			addToCart: addToCart,
			clearCart: clearCart,
			updateCart: updateCart
		}

		return services;

		function addToCart(product) { 
			var addNew = true;

			if ( !cart.length ){
				product.qty = 0;
				cart.push( product );
			}

			angular.forEach( cart , function ( value , key ) {
				// TODO: change name to id, database connect
				if ( value.name == product.name ){
					addNew = false;
					cart[key].qty++;
				}
			});

			if ( addNew ){
				product.qty = 1;
				cart.push( product );
			}

			store.set( 'cart' , getCart() );

		}

		function clearCart() {
			store.remove( 'cart' );
			cart.length = 0;			
		}
		
		function getCart() {
			return cart;
		}

		function updateCart(newCart){			
			store.set( 'cart' , newCart );
		}
	}

})();