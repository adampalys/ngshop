(function(){

	'use strict'

	angular
		.module('app')
		.service('usersService', usersService);

	usersService.$inject = ['$http'];

    function usersService($http){

        var services = {
                getUser: getUser,
                getUsers: getUsers
        }

        return services;

        function getUser(index){
            return $http.get( 'model/users.json' ).then(success,error);

            function success(users){
                return users.data[index];
            }

            function error(){
                console.log( 'Błąd pobrania pliku json' );
            }
        }

        function getUsers(){
            return $http.get( 'model/users.json' ).then(success,error);

            function success(users){
                return users.data;
            }

            function error(){
                console.log( 'Błąd pobrania pliku json' );
            }
        }

    }

})();