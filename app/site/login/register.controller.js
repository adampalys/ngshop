(function(){

	'use strict'

	angular
		.module('app.login')
		.controller('RegisterController', RegisterController);

	function RegisterController(){

		var vm = this;
		vm.input = {};
		vm.formSubmit = formSubmit;

		function formSubmit() {
			vm.errors = {};
			vm.errors.email = 'Przykładowy błąd';
			vm.submit = true;
			console.log( vm.input );
			//TODO: Authorization
		}
	}
	
})();