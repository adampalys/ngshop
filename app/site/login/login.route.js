(function(){
 
    'use strict';

    angular
        .module('app')
        .config(config);

    function config( $routeProvider , $httpProvider ){
        
        $routeProvider
            .when('/login',{
                controller: 'LoginController',
                controllerAs: 'vm',
                templateUrl: 'app/site/login/login.html'
        });

        $routeProvider
            .when('/register',{
                controller: 'RegisterController',
                controllerAs: 'vm',
                templateUrl: 'app/site/login/register.html'
        });
    }
    
})();
