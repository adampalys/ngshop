(function(){

	'use strict'

	angular
		.module('app.orders')
		.controller('OrdersController', OrdersController);

	OrdersController.$inject = ['orders'];

	function OrdersController(orders){
		var vm = this;
		vm.orders = orders;
	}

})();