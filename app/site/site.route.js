(function(){
 
    'use strict';

    angular
        .module('app')
        .config(config);

    function config( $routeProvider , $httpProvider ){
    
        $routeProvider
            .when('/cart',{
                controller: 'CartController',
                controllerAs: 'vm',
                templateUrl: 'app/site/cart/cart.html',
                resolve:{
                    cart: ['cartFactory', function (cartFactory){
                        return cartFactory;
                    }]
                }
        });

        $routeProvider
            .when('/orders',{
                controller: 'OrdersController',
                controllerAs: 'vm',
                templateUrl: 'app/site/orders/orders.html',
                resolve: {
                    orders: ['ordersService', function (ordersService){
                        return ordersService.getOrders();
                    }]
                }
        });

    }

})();