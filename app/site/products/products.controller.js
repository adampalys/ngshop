(function(){

	'use strict'

	angular
		.module('app.products')
		.controller('ProductsController', ProductsController);

	ProductsController.$inject = ['products', 'cart'];

	function ProductsController(products, cart){
		var vm = this;
		vm.products = products;
		vm.addToCart = cart.addToCart;
	}

})();