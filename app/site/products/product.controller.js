(function(){

	'use strict'

	angular
		.module('app.products')
		.controller('ProductController', ProductController);

	ProductController.$inject = ['product', 'cart'];
		//console.log('product');

	function ProductController(product, cart) {
		var vm = this;
		vm.product = product;	
		vm.addToCart = cart.addToCart;	
	}

})();