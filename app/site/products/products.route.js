(function(){
 
    'use strict';

    angular
        .module('app')
        .config(config);

    function config( $routeProvider , $httpProvider ){

         $routeProvider
            .when('/products',{
                controller: 'ProductsController',
                controllerAs: 'vm',
                templateUrl: 'app/site/products/products.html',
                resolve: {
                    products: ['productsService', function (productsService){
                        return productsService.getProducts();
                    }],
                    cart: ['cartFactory', function (cartFactory){
                        return cartFactory;
                    }]
                }
        });

        $routeProvider
            .when('/product/:id', {
                controller: 'ProductController',
                controllerAs: 'vm',
                templateUrl: 'app/site/products/product.html',
                resolve: {
                    product: ['productsService', '$route', function (productsService, $route){
                        return productsService.getProduct($route.current.params.id);
                    }],
                    cart: ['cartFactory', function (cartFactory){
                        return cartFactory;
                    }]
                }
        });
        
    }   	

})();
