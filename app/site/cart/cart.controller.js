(function(){

	'use strict'

	angular
		.module('app.site')
		.controller('CartController', CartController);

	CartController.$inject = ['$scope','cart'];

	function CartController($scope,cart){
		var vm = this;

		vm.alert = {};
		vm.cart = cart;
		vm.cart.removeItem = removeItem;
		vm.setOrder = setOrder;
		vm.cart.total = total;
		vm.update = update;

		function update(){
			vm.cart.updateCart( vm.cart.getCart() );
		}

		function removeItem($index){
			vm.cart.getCart().splice( $index , 1 );
			vm.cart.updateCart( vm.cart.getCart() );			
		}

		function setOrder( $event ) {

			// TODO: check if a user is logged in

			var loggedIn = true;
			if ( !loggedIn )
			{
				vm.alert = { type : 'warning' , msg : 'Musisz być zalogowany, żeby złożyć zamówienie.' };
				$event.preventDefault();
				return false;
			}

			// TODO: send to database

			vm.alert = { type : 'success' , msg : 'Zamówienie złożone. Nie odświeżaj strony. Trwa przekierowywanie do płatności...' };
			vm.cart.clearCart();

			$event.preventDefault();
			$( '#paypalForm' ).submit();

		}

		function total(){
			var total = 0;
			angular.forEach( vm.cart.getCart() , function ( item ) {
				total += item.qty * item.price;
			});
			return total;
		}
		
	}

	



})();