'use strict'

angular
	.module('app.site', [
		'ngRoute',
		'app.login',
		'app.orders',
		'app.products'
	]);